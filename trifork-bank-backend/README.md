# Backend project consisting of the bank microservices

## Building the JARs
mvn clean install [-Dmaven.test.skip=true] in ./


## Create docker container
docker build -t <image name> -f <config file name>(default is Dockerfile) <Dockerfile location> // build image

docker create --name transfer-db --add-host -p 9084:5432 postgres  // create container

docker run -d -it --name <container name> -p <portHost>:<portContainer> --network <type> --link <container>:<image> <image>  // create and start container

docker inspect (-f='{{.object}}') <container>  // for container information

docker start <container> // start existing container

docker stop <container> // stop te container

docker ps // show running containers


## Naming conventions
Image names: <ms-name>-dms

Container names: <ms-name>-cms
DB container names: <ms-name>-db


## Running Code Coverage
Code coverage is automatically run when you merge to master

It is possible to run it locally on your branch, To do this you need to do the following:
- run sonarqube in docker on port 9000 by doing: 
docker run -d --name sonarqube -p 9000:9000  sonarqube
- run maven to sonarqube by doing: 
mvn clean install sonar:sonar

This will put the code coverage of the application on your local sonarqube, which 
you can access on 'localhost:9000'. when looking the code coverage you can filter what you see, this includes module filters


## Port mappings 
all ports for the ms are identical, which is 8080, their db ports vary 

- person-ms : 9081
- authentication-ms: 9082
- account-ms: 9083
- transfer-ms: 9084




## IDK
run: $ ./mvnw install && ./mvnw spring-boot:run -pl application
