package nl.trifork.bank.personms.client;
import nl.trifork.bank.personms.model.Transfer;
import nl.trifork.bank.transferms.model.TransferRequest;
import org.springframework.cloud.netflix.feign.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
@FeignClient("transfer-ms")
public interface TransferClient {
    @RequestMapping(method = RequestMethod.POST, value = "accounts/{accountId}/transfers/")
    ResponseEntity<String> createTransfer(@PathVariable("accountId") long accountId, @RequestBody TransferRequest transfer);
}
