package nl.trifork.bank.transferms.model;

import java.util.Set;

public class TransferRequest {

    private long amount;
    private String fromKey;
    private String toKey;
    private String description;
    private long userId;
    private Set<String> scope;

    public TransferRequest(long amount, String fromKey, String toKey, String description, long userId) {
        this.amount = amount;
        this.fromKey = fromKey;
        this.toKey = toKey;
        this.description = description;
        this.userId = userId;
    }

    public TransferRequest() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getFromKey() {
        return fromKey;
    }

    public void setFromKey(String fromKey) {
        this.fromKey = fromKey;
    }

    public String getToKey() {
        return toKey;
    }

    public void setToKey(String toKey) {
        this.toKey = toKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setScope(Set<String> scope) {
        this.scope = scope;
    }

    public Set<String> getScope() {
        return scope;
    }
}
