package nl.trifork.bank.personms.service;

import nl.trifork.bank.personms.DAO.PersonDAO;
import nl.trifork.bank.personms.client.AccountClient;
import nl.trifork.bank.personms.client.TransferClient;
import nl.trifork.bank.personms.exception.UserAlreadyExistException;
import nl.trifork.bank.personms.exception.UserNotFoundException;
import nl.trifork.bank.personms.model.Account;
import nl.trifork.bank.personms.model.Person;
import nl.trifork.bank.personms.model.Role;
import nl.trifork.bank.personms.model.Transfer;
import nl.trifork.bank.transferms.model.TransferRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PersonService {

    private final long ADMIN_ID = 1l;

    private PersonDAO personDAO;
    private AccountClient accountClient;
    private TransferClient transferClient;
    private Account account;
    private final long firstAmount = 100;
    private final String description = "This is the automatic created account of ";
    private final String adminEmail = "info@seapbank.nl";


    @Autowired
    public PersonService(PersonDAO personDAO, AccountClient accountClient, TransferClient transferClient) {
        this.personDAO = personDAO;
        this.accountClient = accountClient;
        this.transferClient = transferClient;
    }

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    /*------------------------ CREATE PERSON METHODS ----------------------*/

    /**
     * Method to create Person with the person service and writes to DB
     *
     * @param person that is to be created
     * @return the person that is created in JSON format
     */
    public Person createPerson(Person person) throws UserAlreadyExistException {
        String email = person.getEmail();
        if(personDAO.findPersonByEmail(email) != null) throw new UserAlreadyExistException("FAILED to create user --> ALREADY EXISTS");

        String password = passwordEncoder.encode(person.getPassword()); //encode user pass
        Person personObj = new Person(email, password, person.getFirstName(), person.getLastName(), Role.USER, true);

        long id = personObj.getId();

        personDAO.save(personObj); //save user

        String name = personObj.getFirstName() + " " + personObj.getLastName();
        Account account1 = accountClient.create(new Account(name, description, id, 0, 0 )).getBody();
//        Account account1 = ;

        System.out.println("PERSON account created: {id} , {key}" + account1.getId() + " - " + account1.getKey());

        Person admin = personDAO.findPersonByEmail(adminEmail); //Find admin account with ID 1
        ResponseEntity<List<Account>> adminResponse = accountClient.findByUser(person.getId()); //Find admin account with ID 1
        ResponseEntity<List<Account>> userResponse = accountClient.findByUser(id); //Find user account
        TransferRequest transfer = new TransferRequest(firstAmount, adminResponse.getBody().get(0).getKey(), account1.getKey(),
                "Welkoms cadeau", admin.getId());
        System.out.println("PERSON transfer object: amount:, adminAcckey;, userAccId:, adminkey:" +
                transfer.getAmount() + " - " + transfer.getFromKey() + " - " + transfer.getToKey() + " - " + transfer.getUserId());
        transferClient.createTransfer(account1.getId(), transfer);

        return personObj;
    }

    public Person createAdmin(Person person) throws UserAlreadyExistException {
        String email = person.getEmail();
//        if(personDAO.findPersonByEmail(email) != null) throw new UserAlreadyExistException("FAILED to create user --> ALREADY EXISTS");

        personDAO.save(person);
        account = new Account("ADMIN ACCOUNT",  "ADMIN ACCOUNT TO TRANSFER TO NEW USERS", person.getId(), Long.MIN_VALUE, 0);
        System.out.println("CREATING ADMIN ACCOUNT FROM PERSON:createAdmin with new account:" + account.getId());
        accountClient.create(account);
        return person;
    }

    /*------------------------ READ PERSON METHODS ----------------------*/

    /**
     * Method that finds person from given id from DB
     *
     * @param id from person to search for
     * @return person
     * @throws UserNotFoundException if person is not found
     */
    public Person findPersonById(long id) throws UserNotFoundException {
        if (personDAO.exists(id)) {
            return personDAO.findPersonById(id);
        }
        throw new UserNotFoundException();
    }

    /**
     * Method that finds all person from DB
     *
     * @return All persons from DB table
     * @throws UserNotFoundException if no persons have been found
     */
    public List<Person> findAll() throws UserNotFoundException {
        if (personDAO.findAll() != null) {
            return (List<Person>) personDAO.findAll();
        }
        throw new UserNotFoundException();
    }

    /**
     * method that finds person from DB from given email
     *
     * @param email from person to be found
     * @return person
     * @throws UserNotFoundException if the person is not found
     */
    public Person findPersonByEmail(String email) throws UserNotFoundException {
        if (personDAO.findPersonByEmail(email) != null) {
            return personDAO.findPersonByEmail(email);
        }
        throw new UserNotFoundException();
    }

    /*------------------------ UPDATE PERSON METHODS ----------------------*/

    public Person update(Person person) {
        if (personDAO.exists(person.getId())) {
            personDAO.save(person);
        }
        return null;
    }

    /*------------------------ DELETE PERSON METHODS ----------------------*/

    /**
     * method that deletes person from DB from given ID
     *
     * @param id from person that is to be deleted
     * @return id from deleted person
     * @throws UserNotFoundException if person from given ID does not exists
     */
    public long delete(long id) throws UserNotFoundException {
        if (personDAO.exists(id)) {
            personDAO.delete(id);
            return id;
        }
        throw new UserNotFoundException();
    }
}
