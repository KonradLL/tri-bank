package nl.trifork.bank.personms.controller;

import nl.trifork.bank.personms.exception.UserAlreadyExistException;
import nl.trifork.bank.personms.exception.UserNotFoundException;
import nl.trifork.bank.personms.model.Person;
import nl.trifork.bank.personms.model.oauth.PersonAuthentication;
import nl.trifork.bank.personms.service.PersonService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/person")
public class MainController {

    private PersonService personService;
    final static Logger logger = Logger.getLogger(MainController.class);

    /**
     * Constructor personService
     *
     * @param personService
     */
    @Autowired
    public void setPersonService(PersonService personService) {

        this.personService = personService;
    }

    /*------------------------ MAIN CONTROLLER METHODS ----------------------*/

    /**
     * Method to test if the service works
     *
     * @return String containing Hello World!
     */
    @RequestMapping("/index")
    public String account() {
        return "Hello world!";
    }

    /*------------------------ CREATE CONTROLLER METHODS ----------------------*/
    /**
     * Method to create a person entity
     *
     * @param person
     * @return a created person, null if person already exists in DB
     */
    @Transactional
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Person> create(@RequestBody Person person) {
        Set<String> scope   = ((PersonAuthentication) SecurityContextHolder.getContext().getAuthentication()).getScopes();

        person.setCreated_at(Calendar.getInstance().getTime());
        try {
            personService.createPerson(person);
            logger.info("person ADDED: " + person);
            return new ResponseEntity<Person>(person, HttpStatus.CREATED);
        } catch (UserAlreadyExistException ex){
            logger.info("FAILED to created person: " + person + ", Person already exists");
            return new ResponseEntity<Person>(person, HttpStatus.CONFLICT);
        }
    }

    /*------------------------ FIND CONTROLLER METHODS ----------------------*/

    /**
     * Method to find person from DB with given ID
     *
     * @param id
     * @return Person from DB
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Person> getById(@PathVariable(value = "id") int id) throws UserNotFoundException {
        Person person = personService.findPersonById(id);
        if (person == null){
            logger.info("Person with id " + id + " does not exist");
            return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
        }
        logger.info("Found person: " + person);
        return new ResponseEntity<Person>(person, HttpStatus.OK);
    }

    /**
     * Method to find person from DB with given email
     *
     * @param email
     * @return person with that email
     */
    @RequestMapping(value = "/email/{email.+}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Person> getByEmail(@PathVariable(value = "email.+") String email) throws UserNotFoundException {
        Person person = personService.findPersonByEmail(email);
        if (person == null){
            logger.info("Person with email " + email + " does not exist");
            return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
        }
        logger.info("Found person: " + person);

        return new ResponseEntity<Person>(person, HttpStatus.OK);
    }

    /**
     * Method to get all existing user from DB
     *
     * @return all users from DB
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Person>> findAll() throws UserNotFoundException {
        List<Person> persons = personService.findAll();
        if (persons == null){
            logger.info("Persons does not exist");
            return new ResponseEntity<List<Person>>(persons, HttpStatus.NO_CONTENT);
        }
        logger.info("Found " + persons.size() + " persons");
        logger.info(persons);
        logger.info(Arrays.toString(persons.toArray()));
        return new ResponseEntity<List<Person>>(persons, HttpStatus.OK);
    }

    /*------------------------ DELETE CONTROLLER METHODS ----------------------*/

    /**
     * Method to delete person entity from DB
     *
     * @param id from person that is to be deleted
     */
    /*@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable(value = "id") long id) throws UserNotFoundException {
        Person person = personService.findPersonById(id);
        if (person == null){
            logger.info("Person with id " + id + " does not exist");
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            personService.delete(id);
            logger.info("Person with id " + id + " deleted");
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
    }*/

    /*------------------------ UPDATE CONTROLLER METHODS ----------------------*/

    /*@RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Void> update (@RequestBody Person person) throws UserNotFoundException {
        long id = person.getId();
        Person updatePerson = personService.findPersonById(id);
        if (updatePerson == null) {
            logger.info("Person with id " + id + " does not exist");
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            personService.update(person);
            logger.info("Person with id " + id + " is updated");
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
    }*/

}
