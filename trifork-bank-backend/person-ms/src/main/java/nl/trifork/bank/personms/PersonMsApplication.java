package nl.trifork.bank.personms;

import nl.trifork.bank.personms.exception.UserAlreadyExistException;
import nl.trifork.bank.personms.exception.UserNotFoundException;
import nl.trifork.bank.personms.service.PersonService;
import org.aspectj.lang.annotation.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.security.oauth2.client.OAuth2ClientContext;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import nl.trifork.bank.personms.model.Person;
import nl.trifork.bank.personms.model.Role;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@ComponentScan
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@EnableFeignClients
@EnableCircuitBreaker
public class PersonMsApplication {

    @Autowired
    private static PersonService personService;

    @Autowired
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }


    private static final String email       = "info@seapbank.nl";
    private static final String lastName    = "Trifork";
    private static final String firstName   = "LarsThomChrisKonradDouweMike";
    private static final String password    = "theBestPasswordForAAdminMLTCD";
    private static final Role role          = Role.ADMIN;
    private static final boolean enabled    = true;
    private static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    public static void main(String[] args) {
		SpringApplication.run(PersonMsApplication.class, args);
        createAdmin();

	}

	public static void createAdmin() {
        System.out.println("PersonMS-Creating admin");
        String encryptedPassword = passwordEncoder.encode(password);
        Person admin = new Person(email, encryptedPassword, firstName, lastName, role, enabled);

        try {
            personService.createAdmin(admin);
        } catch (UserAlreadyExistException e) {
            System.out.println("nope");
        }
    }

}
