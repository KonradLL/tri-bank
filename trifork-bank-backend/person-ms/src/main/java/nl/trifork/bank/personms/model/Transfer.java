package nl.trifork.bank.personms.model;


import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import javax.persistence.*;
import java.util.Date;

/**
 * Transfer entity Class.
 */
public class Transfer {

    private long id;
    private long amount;
    private long fromAccountId;
    private long toAccountId;
    private long userId;
    private String description;
    private Date createdAt;


    protected Transfer() { }

    public Transfer(long amount, long fromAccountId, long toAccountId, long userId, String description, Date createdAt) {
        this.amount = amount;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.userId = userId;
        this.description = description;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getFromAccountId() { return fromAccountId; }

    public void setFromAccountId(long fromAccountId) { this.fromAccountId = fromAccountId; }

    public long getToAccountId() { return toAccountId; }

    public void setToAccountId(long toAccountId) { this.toAccountId = toAccountId; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
