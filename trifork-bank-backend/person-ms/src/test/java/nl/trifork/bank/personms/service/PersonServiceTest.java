package nl.trifork.bank.personms.service;

import nl.trifork.bank.personms.DAO.PersonDAO;
import nl.trifork.bank.personms.client.AccountClient;
import nl.trifork.bank.personms.client.TransferClient;
import nl.trifork.bank.personms.exception.UserAlreadyExistException;
import nl.trifork.bank.personms.exception.UserNotFoundException;
import nl.trifork.bank.personms.model.Account;
import nl.trifork.bank.personms.model.Person;
import nl.trifork.bank.personms.model.Role;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;
import java.util.Arrays;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@SpringBootTest
public class PersonServiceTest {
    @InjectMocks
    private Logger logger = LoggerFactory.getLogger(PersonService.class);
    private final long KEY_1 = 111111111;
    @Mock
    private PersonService personService;
    @Mock
    private AccountClient accountClient;
    @Mock
    private TransferClient transferClient;
    @Mock
    private Person person;
    @Mock
    private Account account;
    @Mock
    private PersonDAO personDAO;
    @Mock
    private WebRequest webRequest;
    @Mock
    private ResponseEntity response;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        personService = new PersonService(personDAO, accountClient, transferClient);
        webRequest = mock(WebRequest.class);
        response = mock(ResponseEntity.class);
        person = new Person(1l,
                "test@hva.nl",
                "test",
                "je moeder",
                "de tester",
                Role.USER,
                true);
    }
    /*@Test
    public void WhenCreate_andValid_thenReturnReponseEntity() throws UserAlreadyExistException {
        account = new Account();
        response =  new ResponseEntity(account, HttpStatus.CREATED);
        given(personDAO.findPersonByEmail(anyString())).willReturn(null);
        given(personDAO.save(any(Person.class))).willReturn(person);
        when(accountClient.create(anyString(), any(Account.class)))
                .thenReturn(response);
        Person personNew = personService.createPerson(person);
        assertThat(personNew.getFirstName()).isEqualTo(person.getFirstName());
        verify(personDAO).save(person);
    }*/
    @Test
    public void whenCreate_andInvalid_thenReturnReponseEntity() throws UserAlreadyExistException {
    }
    /*@Test
    public void whenFind_andExist_thenReturnPerson() throws UserNotFoundException {
        Person person = new Person(1l, "test1@hva.nl", "test1", "test1", "test1", Role.USER, true);
        person.setId(KEY_1);
        given(personDAO.exists(person.getId())).willReturn(true);
        given(personDAO.findPersonById(person.getId())).willReturn(person);
        Person pers = personService.findPersonById(person.getId());
        assertThat(pers.getId()).isEqualTo(person.getId());
        assertThat(pers.getFirstName()).isEqualTo(person.getFirstName());
    }
    @Test
    public void whenFindAll_andPersonsExist_thenReturnAllPersons() {
        Person person1 = new Person(1l, "test1@hva.nl", "test1", "test1", "test1", Role.USER, true);
        Person person2 = new Person(2l, "test2@hva.nl", "test2", "test2", "test2", Role.USER, true);
        Person person3 = new Person(3l, "test3@hva.nl", "test3", "test3", "test3", Role.USER, true);
        Person personList[] = {person1, person2, person3};
        Iterable<Person> personIterable = Arrays.asList(personList);
        given(personDAO.findAll()).willReturn(personIterable);
        Iterable<Person> persons = personDAO.findAll();
        assertThat(persons).contains(person1, person2, person3);
        assertThat(persons.iterator().next().getFirstName().equals(person1.getFirstName()));
    }*/
}
