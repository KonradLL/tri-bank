package nl.trifork.bank.transferms.DAO;

import nl.trifork.bank.transferms.model.Transfer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransferDAO extends CrudRepository<Transfer, Long> {

    Iterable<Transfer> findAllByUserId(long id);

    Transfer findFirstByOrderByIdDesc();

    Optional<Transfer> findById(long id);

    Iterable<Transfer> findAllByFromAccountIdOrToAccountId(long fromAccountId, long toAccountId);
}
