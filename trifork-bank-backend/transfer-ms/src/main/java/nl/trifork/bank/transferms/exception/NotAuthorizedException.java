package nl.trifork.bank.transferms.exception;

public class NotAuthorizedException extends TransferException {
    public NotAuthorizedException() {
    }

    public NotAuthorizedException(String message) {
        super(message);
    }

    public NotAuthorizedException(Throwable cause) {
        super(cause);
    }

    public NotAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }
}
