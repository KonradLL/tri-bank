package nl.trifork.bank.transferms.utils;

import nl.trifork.bank.transferms.client.AccountClient;
import nl.trifork.bank.transferms.exception.*;
import nl.trifork.bank.transferms.model.Account;
import nl.trifork.bank.transferms.model.Transfer;
import nl.trifork.bank.transferms.model.TransferRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Set;

/**
 * Builds a transfer while first trying to verify all different parts of the transfer. When a validation fails a transfer exception is thrown.
 */
public class TransferBuilder {
    private TransferRequest transferRequest;
    @Autowired
    private AccountClient accountClient;
    private ResponseEntity<Account> issuer;
    private ResponseEntity<Account> receiver;
    private long amount;
    private long userId;

    public ResponseEntity<Account> getIssuer() {
        return issuer;
    }


    public TransferBuilder(TransferRequest transferRequest, AccountClient accountClient) {
        this.accountClient = accountClient;
        this.transferRequest = transferRequest;
        this.amount = transferRequest.getAmount();
        this.issuer = findByKey(transferRequest.getFromKey());
        this.receiver = findByKey(transferRequest.getToKey());
        this.userId = transferRequest.getUserId();
    }

    /**
     * Method for checking all the transfer attributes if they are valid.
     * The method checks if the issued accounts of the transfer exist, the response from the accountMS is OK,
     * is authorized, keys are not identical, amount is valid and if the transfer amount is legit for the issuer
     * to invoke.
     *
     * @return A validated TransferBuilder object that can be used to build a Transfer object.
     */
    public TransferBuilder verify() {
        // Verify if the account's are legit
        if (receiver.getBody() == null) {
            throw new AccountNotFoundException("The receiver account doesn't exist");
        } else if (issuer.getBody() == null) {
            throw new AccountNotFoundException("The issuer account doesn't exist");
        }
        // If the status codes aren't 200 in some way, then throw an error
        if (issuer.getStatusCode() != HttpStatus.OK || receiver.getStatusCode() != HttpStatus.OK) {
            throw new TransferException("The status codes are not valid");
        }
        // Throw exception when the account keys of the transfer are identical
        if (transferRequest.getFromKey().equals(transferRequest.getToKey())) {
            throw new TransferException("The account keys of the transfer are identical");
        }
        // Check if the amount is a valid amount
        if (transferRequest.getAmount() <= 0) {
            throw new IllegalAmountException("Transfer amount can't be zero or less than zero");
        }

        // Check if the transfer amount is legit for the issuer to invoke
        if ((issuer.getBody().getBalance() - transferRequest.getAmount()) < issuer.getBody().getMinBalance()) {
            throw new InsufficientFundsException("Balance is too low");
        }
        return this;
    }


    private ResponseEntity<Account> findByKey(String key) {
        System.out.println("BUILDER KEY:" + key);
        return accountClient.findByKey(key);
    }

    public Transfer build() {
        return new Transfer(amount, issuer.getBody().getId(), receiver.getBody().getId(), userId, transferRequest.getDescription(), new Date());
    }
}
