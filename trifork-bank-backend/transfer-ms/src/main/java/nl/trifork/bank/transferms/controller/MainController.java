package nl.trifork.bank.transferms.controller;

import nl.trifork.bank.transferms.client.AccountClient;
import nl.trifork.bank.transferms.model.Account;
import nl.trifork.bank.transferms.model.Person;
import nl.trifork.bank.transferms.model.Transfer;
import nl.trifork.bank.transferms.model.TransferRequest;
import nl.trifork.bank.transferms.model.oauth.PersonAuthentication;
import nl.trifork.bank.transferms.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Set;


@RefreshScope
@RestController
@RequestMapping("accounts/{accountId}/transfers")
public class MainController {

    private TransferService transferService;
    private final static Logger logger = LoggerFactory.getLogger(MainController.class);
    private AccountClient accountClient;


    @Autowired
    public MainController(TransferService transferService, AccountClient accountClient) {
        this.transferService = transferService;
        this.accountClient = accountClient;
    }


    /**
     * Test method for checking if the microservice works
     *
     * @return String containing hello world
     */
    @RequestMapping("/hello")
    public String helloworld() {
        return "Hello world!";
    }


    /**
     * Creates a transferms from the input of the body of the request.
     *
     * @param request TransferRequest object
     * @return The transferms that has just been saved
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> createTransfer(@RequestBody TransferRequest request) {
        Set<String> scope = ((PersonAuthentication) SecurityContextHolder.getContext().getAuthentication()).getScopes();
        Person person = ((PersonAuthentication) SecurityContextHolder.getContext().getAuthentication()).getDetails();
        if (person == null && scope.contains("full")) {
            logger.info("PERSON == NULL, make account for new user if");
            return transferService.createTransfer(request);
        } else if (person == null) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        if (scope.contains("admin") || scope.contains("full")) {
            return transferService.createTransfer(request);
        } else if (scope.contains("user")) {
            if (person.getId() == request.getUserId()) {
                return transferService.createTransfer(request);
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }


    /**
     * Reads a transferms from the database by using the id from the request resource to find it.
     *
     * @param id Transfer id
     * @return The transfer that matches the id that has been requested
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Transfer findTransferById(@PathVariable Long id) {
        return transferService.findTransfer(id);
    }


    /**
     * @param accountId idof the account
     * @return An Iterable of all the transfers executed by the user
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Transfer>> findTransfersForAccount(@PathVariable Long accountId) {
        Set<String> scope = ((PersonAuthentication) SecurityContextHolder.getContext().getAuthentication()).getScopes();
        Person person = ((PersonAuthentication) SecurityContextHolder.getContext().getAuthentication()).getDetails();
        if (scope.contains("admin") || scope.contains("full")) {
            return ResponseEntity
                    .ok()
                    .body(transferService.findTransfersForAccountId(accountId));
        } else if (scope.contains("user")) {
            Account account = accountClient.findByAccountId(accountId).getBody();
            if (account.getUserId() == person.getId()) {
                return ResponseEntity
                        .ok()
                        .body(transferService.findTransfersForAccountId(accountId));
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
