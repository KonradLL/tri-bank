package nl.trifork.bank.transferms;

import nl.trifork.bank.transferms.DAO.TransferDAO;
import nl.trifork.bank.transferms.client.AccountClient;
import nl.trifork.bank.transferms.exception.*;
import nl.trifork.bank.transferms.model.Account;
import nl.trifork.bank.transferms.model.Transfer;
import nl.trifork.bank.transferms.model.TransferRequest;
import nl.trifork.bank.transferms.model.oauth.PersonAuthentication;
import nl.trifork.bank.transferms.service.TransferService;
import nl.trifork.bank.transferms.utils.TransferBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class TransferServiceTest {


    @InjectMocks
    private TransferService subject;
    private String issuerKey = "273272020";
    private String receiverKey = "010486556";
    private long issuerAccountId = 0;
    private long receiverAccountId = 1;
    private ResponseEntity<Account> issuer;
    private HttpHeaders httpHeaders;
    private Transfer transfer;
    private TransferRequest transferRequest;
    private Account successAccount;
    private ResponseEntity<Account> receiver;

    @Mock
    private TransferDAO transferDAO;

    @Mock
    private AccountClient accountClient;

    @Mock
    private PersonAuthentication personAuthentication;


    @Before
    public void setUp() {
        initMocks(this);
        httpHeaders = new HttpHeaders();
        httpHeaders.setETag("\"1\"");
        transferRequest = new TransferRequest(10, issuerKey, receiverKey, "test request");
        transfer = new Transfer(10, issuerAccountId, receiverAccountId, 10, "test request", null);
        successAccount = new Account();
        successAccount.setUserId(transfer.getUserId());
        subject = new TransferService(transferDAO, accountClient);
        Set<String> scope = new HashSet<>();
        scope.add("full");
        when(personAuthentication.getScopes()).thenReturn(scope);


    }


    @Test
    public void shouldFail() {
    }
    /*@Test
    public void shouldReturnFilledTransferObject() {
        Transfer transfer = new Transfer();
        long id = 1;
        transfer.setUserId(id);
        when(transferDAO.findOne(id))
                .thenReturn(transfer);
        Transfer subjectTransfer = subject.findTransfer(1);
        assertThat(transfer.getId(), is(subjectTransfer.getId()));
    }

    @Test
    public void shouldReturnAllTransfers() {
        Iterable<Transfer> transfers = mock(Iterable.class);
        when(subject.findAllTransfers()).thenReturn(transfers);
        subject.findAllTransfers();
        verify(transferDAO).findAll();
    }

    @Test
    public void shouldReturnAllTransfersForUserId() {

        Iterable<Transfer> transfers = mock(Iterable.class);
        when(subject.findTransfersForUser(10)).thenReturn(transfers);
        subject.findTransfersForUser(10);
        verify(transferDAO).findAllByUserId(10);
    }


    @Test
    public void shouldReturnNullIfThereIsNoneWithId() {

        given(transferDAO.findOne(anyLong()))
                .willReturn(null);
        Transfer subjectTransfer = subject.findTransfer(120203094);
        assertThat(subjectTransfer, is(nullValue()));
    }


    @Test
    public void shouldCreateTransferSuccessfully() {
        Account issuerAccount = new Account();
        issuerAccount.setVersion(0);
        issuerAccount.setUserId(10);
        issuerAccount.setId(0);
        Account receiverAccount = new Account();
        receiverAccount.setId(1);
        receiverAccount.setUserId(10);
        receiverAccount.setVersion(0);
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(receiverAccount);
        issuerAccount.setId(0);
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(issuerAccount);
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        issuerAccount.setBalance(1000);
        Transfer transfer = new TransferBuilder(transferRequest, accountClient).verify().build();
        transfer.setCreatedAt(null);
//        when(accountClient.executeTransfer(String.valueOf(issuerAccount.getVersion()), transfer)).thenReturn(ResponseEntity.ok().build());
//        subject.createTransfer(transferRequest);
//        verify(transferDAO).save(transfer);
    }
    @Test(expected = NotAuthorizedException.class)
    public void shouldThrowNotAuthorizedExceptionWhenTheUserIdIsNotEqualToTransferUserId() {
        Account issuerAccount = new Account();
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(new Account());
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(issuerAccount);
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        when(accountClient.executeTransfer(httpHeaders.getETag(), transfer)).thenReturn(ResponseEntity.ok().build());
        issuerAccount.setBalance(1000);
        subject.createTransfer(transferRequest);
        verify(transferDAO).save(transfer);
    }


    @Test(expected = TransferException.class)
    public void shouldThrowTransferExceptionWhenTransferWithIdenticalKeys() {
        Account issuerAccount = new Account();
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(issuerAccount);
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(issuerAccount);
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        when(accountClient.executeTransfer(httpHeaders.getETag(), transfer)).thenReturn(ResponseEntity.ok().build());
        issuerAccount.setBalance(1000);
        transfer.setToAccountId(transfer.getFromAccountId());
        subject.createTransfer(transferRequest);
    }

    @Test(expected = TransferException.class)
    public void shouldThrowTransferExceptionWhenExecutingTransferHadFailed() {
        Account issuerAccount = new Account();
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(issuerAccount);
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(issuerAccount);
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        when(accountClient.executeTransfer(httpHeaders.getETag(), transfer)).thenReturn(ResponseEntity.badRequest().build());
        issuerAccount.setBalance(1000);
        subject.createTransfer(transferRequest);
    }


    @Test(expected = AccountNotFoundException.class)
    public void shouldThrowAccountNotFoundExceptionWhenIssuerResponseEntityHasNoBody() {
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .build();
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(new Account());
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        subject.createTransfer(transferRequest);
    }

    @Test(expected = AccountNotFoundException.class)
    public void shouldThrowAccountNotFoundExceptionWhenIssuerResponseEntityHasNoBodyy() {
        when(accountClient.findByKey(receiverKey)).thenThrow(Exception.class);
        subject.createTransfer(transferRequest);
    }

    @Test(expected = AccountNotFoundException.class)
    public void shouldThrowAccountNotFoundExceptionWhenReceiverResponseEntityHasNoBody() {
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(new Account());
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .build();
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        subject.createTransfer(transferRequest);
    }

    @Test(expected = InsufficientFundsException.class)
    public void shouldThrowInsufficientFundsExceptionWhenBalanceIsTooLow() {
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(successAccount);
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(new Account());
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        subject.createTransfer(transferRequest);
    }

    @Test(expected = TransferException.class)
    public void shouldThrowTransferExceptionWhenIssuerHasNoETag() {
        issuer = ResponseEntity
                .ok()
                .body(new Account());
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(new Account());
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        subject.createTransfer(transferRequest);
    }

    @Test(expected = TransferException.class)
    public void shouldThrowTransferExceptionWhenHttpStatusOfIssuerAndReceiverIsNotOK() {
        issuer = ResponseEntity
                .badRequest()
                .body(new Account());
        receiver = ResponseEntity
                .badRequest()
                .headers(httpHeaders)
                .body(new Account());
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        subject.createTransfer(transferRequest);
    }

    @Test(expected = TransferException.class)
    public void shouldThrowTransferExceptionWhenHttpStatusOfReceiverIsNotOK() {
        issuer = ResponseEntity
                .ok()
                .body(new Account());
        receiver = ResponseEntity
                .badRequest()
                .headers(httpHeaders)
                .body(new Account());
        when(accountClient.findByKey(transferRequest.getFromKey())).thenReturn(issuer);
        when(accountClient.findByKey(transferRequest.getToKey())).thenReturn(receiver);
        subject.createTransfer(transferRequest);
    }

    @Test(expected = IllegalAmountException.class)
    public void shouldThrowTransferExceptionWhenTransferAmountIsNotValid() {
        transferRequest.setAmount(-10);
        receiver = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(new Account());
        issuer = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(successAccount);
        when(accountClient.findByKey(issuerKey)).thenReturn(issuer);
        when(accountClient.findByKey(receiverKey)).thenReturn(receiver);
        when(accountClient.executeTransfer(httpHeaders.getETag(), transfer)).thenReturn(ResponseEntity.ok().build());
        successAccount.setBalance(1000);
        subject.createTransfer(transferRequest);
        verify(transferDAO).save(transfer);
    }

*/
}
