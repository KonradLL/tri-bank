package nl.trifork.bank.transferms;

import nl.trifork.bank.transferms.DAO.TransferDAO;
import nl.trifork.bank.transferms.client.AccountClient;
import nl.trifork.bank.transferms.model.Account;
import nl.trifork.bank.transferms.model.Transfer;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.netflix.feign.FeignContext;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Random;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransferServiceIntegrationTests {

    @Autowired
    private TransferDAO transferDAO;

    @MockBean
    private AccountClient accountClient;

    @After
    public void tearDown() throws Exception {
        transferDAO.deleteAll();
    }

    @Test
    public void shouldSaveAndFetchTransfer() {
        int randomInt = new Random().nextInt();
        System.out.println("randomint: " + randomInt);
        Transfer transferA = new Transfer(10, 0, 1, randomInt, "School money", new Date());
        transferDAO.save(transferA);
        Transfer maybeTransferA = transferDAO.findAllByUserId(randomInt).iterator().next();
        assertThat(transferA, is(maybeTransferA));
    }

}
