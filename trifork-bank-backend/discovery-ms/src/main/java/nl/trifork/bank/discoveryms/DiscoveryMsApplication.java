package nl.trifork.bank.discoveryms;

import org.springframework.boot.SpringApplication;
		import org.springframework.boot.autoconfigure.SpringBootApplication;
		import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.context.request.WebRequest;

import java.util.logging.Logger;

@EnableEurekaServer
@SpringBootApplication
public class DiscoveryMsApplication {



	public static void main(String[] args) {
		SpringApplication.run(DiscoveryMsApplication.class, args);
	}






}
