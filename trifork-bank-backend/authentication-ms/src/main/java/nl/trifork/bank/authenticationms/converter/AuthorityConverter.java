package nl.trifork.bank.authenticationms.converter;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;
import java.util.StringJoiner;

@Converter(autoApply = true)
public class AuthorityConverter  implements AttributeConverter<List<GrantedAuthority>, String> {
    @Override
    public String convertToDatabaseColumn(List<GrantedAuthority> grantedAuthorities) {
        StringJoiner sj = new StringJoiner(",");
        for(GrantedAuthority ga : grantedAuthorities) sj.add(ga.getAuthority());
        return sj.toString();
    }

    @Override
    public List<GrantedAuthority> convertToEntityAttribute(String s) {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(s);
    }
}
