package nl.trifork.bank.authenticationms.service;

import feign.Client;
import nl.trifork.bank.authenticationms.DAO.ClientDetailsDAO;
import nl.trifork.bank.authenticationms.model.Person;
import nl.trifork.bank.authenticationms.model.oauth.ClientDetails;

import nl.trifork.bank.authenticationms.model.oauth.RefreshToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.BadClientCredentialsException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ClientDetailsService {

    ClientDetailsDAO clientDetailsDAO;
    PersonService personService;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public ClientDetailsService(ClientDetailsDAO clientDetailsDAO, PersonService personService) {
        this.clientDetailsDAO = clientDetailsDAO;
        this.personService = personService;
    }

    private ClientDetails getClientDetails(String clientId, String clientSecret) {
        if(clientId == null) throw new NoSuchClientException("No client id given.");

        ClientDetails clientDetails = this.get(clientId);

        if(clientDetails == null) throw new NoSuchClientException("Client with client id " + clientId + " could not be found.");

        if(!clientDetails.getClientSecret().equals(clientSecret)) throw new BadClientCredentialsException();

        return clientDetails;
    }

    public ClientDetails getAndVerify(String clientId, String clientSecret) {
        ClientDetails clientDetails = this.getClientDetails(clientId, clientSecret);

        if(!clientDetails.getAuthorizedGrantTypes().contains("client_credentials")) throw new InvalidGrantException("Client " + clientId + " doesn't support the client_credentials grant type.");

        return clientDetails;
    }

    public ClientDetails getAndVerify(String clientId, String clientSecret, String username, String password) {
        ClientDetails clientDetails = this.getClientDetails(clientId, clientSecret);

        if(!clientDetails.getAuthorizedGrantTypes().contains("password")) throw new InvalidGrantException("Client " + clientId + " doesn't support the password grant type.");

        if(username == null || password == null) throw new BadCredentialsException("User credentials are required for password grant type.");

        Person person = this.personService.getByEmail(username);

        if(passwordEncoder.matches(person.getPassword(), password)) throw new BadCredentialsException("The user credentials are incorrect.");

        return clientDetails;
    }

    public ClientDetails getAndVerify(RefreshToken refreshToken) {
        if(refreshToken.getExpiresAt().compareTo(new Date()) == -1) throw new InvalidTokenException("Token has expired.");

        ClientDetails clientDetails = this.clientDetailsDAO.findClientDetailsByClientId(refreshToken.getClientId());

        if(clientDetails == null) throw new NoSuchClientException("No client could be found with refresh token " + refreshToken.getToken() + ".");

        return clientDetails;
    }

    public ClientDetails get(String clientId) {
        return clientDetailsDAO.findClientDetailsByClientId(clientId);
    }

}
