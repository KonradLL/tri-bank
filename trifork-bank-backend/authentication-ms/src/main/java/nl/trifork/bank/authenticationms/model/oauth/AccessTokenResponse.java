package nl.trifork.bank.authenticationms.model.oauth;

public class AccessTokenResponse {

    String access_token;
    String token_type = "Bearer";
    long expires_in;
    String refresh_token;

    public AccessTokenResponse(AccessToken accessToken, ClientDetails clientDetails) {
        this.access_token = accessToken.getToken();
        this.expires_in = clientDetails.getAccessTokenValiditySeconds();
    }

    public AccessTokenResponse(AccessToken accessToken, RefreshToken refreshToken, ClientDetails clientDetails) {
        this.access_token = accessToken.getToken();
        this.refresh_token = refreshToken.getToken();
        this.expires_in = clientDetails.getAccessTokenValiditySeconds();
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }
}
