package nl.trifork.bank.authenticationms.service;

import nl.trifork.bank.authenticationms.DAO.AccessTokenDAO;
import nl.trifork.bank.authenticationms.DAO.ClientDetailsDAO;
import nl.trifork.bank.authenticationms.DAO.RefreshTokenDAO;
import nl.trifork.bank.authenticationms.model.Person;
import nl.trifork.bank.authenticationms.model.Role;
import nl.trifork.bank.authenticationms.model.oauth.AccessToken;
import nl.trifork.bank.authenticationms.model.oauth.ClientDetails;
import nl.trifork.bank.authenticationms.model.oauth.RefreshToken;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.LinkedHashSet;

@Service
public class TokenService {

    SecureRandom sr = new SecureRandom();
    AccessTokenDAO accessTokenDAO;
    RefreshTokenDAO refreshTokenDAO;
    ClientDetailsDAO clientDetailsDAO;

    public TokenService(AccessTokenDAO accessTokenDAO, RefreshTokenDAO refreshTokenDAO, ClientDetailsDAO clientDetailsDAO) {
        this.accessTokenDAO = accessTokenDAO;
        this.refreshTokenDAO = refreshTokenDAO;
        this.clientDetailsDAO = clientDetailsDAO;
    }

    public AccessToken generateAccessToken(ClientDetails clientDetails, Person person) {
        AccessToken accessToken = new AccessToken(this.generateSecureToken(), clientDetails.getAccessTokenValiditySeconds());

        accessToken.setClientId(clientDetails.getClientId());

        LinkedHashSet<String> scopes = new LinkedHashSet<>();

        if(person != null) {
            accessToken.setUsername(person.getEmail());

            switch(person.getRole()) {
                case USER:
                    System.out.println("user");
                    scopes.add("user");
                    break;
                case ADMIN:
                    System.out.println("admin");
                    scopes.add("admin");
                    break;
            }
        } else {
            scopes.add("full");
        }

        accessToken.setScopes(scopes);

        this.insertAccessToken(accessToken);

        return accessToken;
    }

    public RefreshToken generateRefreshToken(ClientDetails clientDetails, AccessToken accessToken) {
        RefreshToken refreshToken = new RefreshToken(this.generateSecureToken(), clientDetails.getRefreshTokenValiditySeconds());

        refreshToken.setClientId(accessToken.getClientId());
        refreshToken.setAccessTokenId(accessToken.getId());

        this.insertRefreshToken(refreshToken);

        return refreshToken;
    }

    public AccessToken tokenIsValid(String token) {
        if(token == null) throw new AccessTokenRequiredException(null);

        AccessToken accessToken = accessTokenDAO.findAccessTokenByToken(token);
        if(accessToken == null) throw new InvalidTokenException("Access token could not be matched.");

        if(accessToken.getExpiresAt().compareTo(new Date()) == -1) throw new InvalidTokenException("Token has expired.");

        return accessToken;
    }

    public AccessToken getAccessTokenById(int id) {
        return this.accessTokenDAO.findOne(id);
    }

    public RefreshToken getRefreshToken(String token) {
        if(token == null) throw new InvalidTokenException("Refresh token must be given.");

        RefreshToken refreshToken = this.refreshTokenDAO.getByToken(token);

        if(refreshToken == null) throw new InvalidTokenException("Refresh token could not be matched.");

        return refreshToken;
    }

    public void deleteAccessToken(int id) {
        this.accessTokenDAO.delete(id);
    }

    public void deleteRefreshToken(int id) {
        this.refreshTokenDAO.delete(id);
    }

    private void insertAccessToken(AccessToken accessToken) {
        this.accessTokenDAO.save(accessToken);
    }

    private void insertRefreshToken(RefreshToken refreshToken) {
        this.refreshTokenDAO.save(refreshToken);
    }

    private String generateSecureToken() {
        return new BigInteger(256, sr).toString(64);
    }

}
