package nl.trifork.bank.authenticationms.model.oauth;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "oauth_refresh_token")
public class RefreshToken {

    public RefreshToken() { }

    public RefreshToken(String token, int expiresIn) {
        this.token = token;

        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, expiresIn);
        this.expiresAt = c.getTime();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    int id;

    @Column(name = "token", nullable = false)
    String token;

    @Column(name = "client_id", nullable = false)
    String clientId;

    @Column(name = "expires_at", nullable = false)
    Date expiresAt;

    @Column(name = "access_token_id", nullable = false)
    int accessTokenId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getExpiresAt() { return expiresAt; }

    public void setExpiresAt(Date expiresAt) { this.expiresAt = expiresAt; }

    public int getAccessTokenId() { return accessTokenId; }

    public void setAccessTokenId(int accessTokenId) { this.accessTokenId = accessTokenId; }
}
