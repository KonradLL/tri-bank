package nl.trifork.bank.authenticationms.DAO;

import nl.trifork.bank.authenticationms.model.oauth.RefreshToken;
import org.springframework.data.repository.CrudRepository;

public interface RefreshTokenDAO extends CrudRepository<RefreshToken, Integer> {

    RefreshToken getByToken(String token);

}
