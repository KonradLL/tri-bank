package nl.trifork.bank.authenticationms.model.oauth;

import nl.trifork.bank.authenticationms.model.Person;
import nl.trifork.bank.authenticationms.model.Role;
import org.springframework.util.StringUtils;

import java.util.Date;

public class CheckTokenResponse {

    long expires_in;
    String authorities;
    String scopes;
    private long id;
    private String email;
    private String firstName;
    private String lastName;
    private Role role;
    private boolean enabled;

    public CheckTokenResponse(AccessToken accessToken, ClientDetails clientDetails, Person person) {
        this.expires_in = (accessToken.getExpiresAt().getTime() - new Date().getTime()) / 1000;
        this.authorities = StringUtils.collectionToCommaDelimitedString(clientDetails.getAuthorities());
        this.scopes = StringUtils.collectionToCommaDelimitedString(accessToken.getScopes());

        if(person != null) {
            this.id = person.getId();
            this.email = person.getEmail();
            this.firstName = person.getFirstName();
            this.lastName = person.getLastName();
            this.role = person.getRole();
            this.enabled = person.getEnabled();
        }
    }

    public long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getAuthorities() { return authorities; }

    public void setAuthorities(String authorities) { this.authorities = authorities; }

    public String getScopes() { return scopes; }

    public void setScopes(String scopes) { this.scopes = scopes; }
}
