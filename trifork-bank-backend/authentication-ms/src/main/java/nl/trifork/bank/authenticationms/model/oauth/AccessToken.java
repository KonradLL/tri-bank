package nl.trifork.bank.authenticationms.model.oauth;

import nl.trifork.bank.authenticationms.converter.CollectionToStringConverter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "oauth_access_token")
public class AccessToken implements Comparable<AccessToken> {

    public AccessToken() { }

    public AccessToken(String token, int expiresIn) {
        this.token = token;

        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, expiresIn);
        this.expiresAt = c.getTime();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    int id;

    @Column(name = "token", nullable = false)
    String token;

    @Column(name = "username")
    String username;

    @Column(name = "client_id", nullable = false)
    String clientId;

    @Column(name = "expires_at", nullable = false)
    Date expiresAt;

    @Convert(converter = CollectionToStringConverter.class)
    @Column(name = "scopes")
    Set<String> scopes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() { return token; }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getExpiresAt() { return expiresAt; }

    public void setExpiresAt(Date expiresAt) { this.expiresAt = expiresAt; }

    public Set<String> getScopes() { return scopes; }

    public void setScopes(Set<String> scopes) { this.scopes = scopes; }

    @Override
    public String toString() {
        return "AccessToken{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", username='" + username + '\'' +
                ", clientId='" + clientId + '\'' +
                ", expiresAt=" + expiresAt +
                ", scopes=" + scopes +
                '}';
    }

    @Override
    public int compareTo(AccessToken accessToken) {
        return this.getExpiresAt().compareTo(accessToken.getExpiresAt());
    }
}
