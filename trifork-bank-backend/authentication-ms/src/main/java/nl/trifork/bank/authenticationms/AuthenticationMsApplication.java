package nl.trifork.bank.authenticationms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
public class AuthenticationMsApplication {
    public static void main(String[] args) {

        SpringApplication.run(AuthenticationMsApplication.class, args);

    }
}
