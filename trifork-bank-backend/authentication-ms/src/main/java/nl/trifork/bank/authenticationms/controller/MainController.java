package nl.trifork.bank.authenticationms.controller;

import nl.trifork.bank.authenticationms.model.Person;
import nl.trifork.bank.authenticationms.model.oauth.*;
import nl.trifork.bank.authenticationms.service.ClientDetailsService;
import nl.trifork.bank.authenticationms.service.PersonService;
import nl.trifork.bank.authenticationms.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.UnsupportedGrantTypeException;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Map;

@RequestMapping("/oauth")
@RestController
public class MainController {

    ClientDetailsService clientDetailsService;
    TokenService tokenService;
    PersonService personService;

    @Autowired
    MainController(ClientDetailsService clientDetailsService, TokenService tokenService, PersonService personService) {
        this.clientDetailsService = clientDetailsService;
        this.tokenService = tokenService;
        this.personService = personService;
    }

    /**
     * Create and obtain an access token
     *
     * @return ResponseEntity<OAuth2AccessToken>
     */
    @Transactional
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public ResponseEntity<AccessTokenResponse> token(@RequestBody Map<String, String> params) {
        RefreshToken rt = null;
        ClientDetails clientDetails;

        if(!params.containsKey("grant_type")) throw new UnsupportedGrantTypeException("Grant type is required.");

        switch(params.get("grant_type")) {
            case "client_credentials":
                clientDetails = this.clientDetailsService.getAndVerify(params.get("client_id"), params.get("client_secret"));
                break;
            case "password":
                clientDetails = this.clientDetailsService.getAndVerify(params.get("client_id"), params.get("client_secret"), params.get("username"), params.get("password"));
                break;
            case "refresh_token":
                rt = this.tokenService.getRefreshToken(params.get("refresh_token"));

                clientDetails = this.clientDetailsService.getAndVerify(rt);

                params.put("username", this.tokenService.getAccessTokenById(rt.getAccessTokenId()).getUsername());
                break;
            default:
                throw new UnsupportedGrantTypeException("Grant type " + params.get("grant_type") + " is not supported.");
        }

        Person person = null;
        if(params.get("username") != null) person = personService.getByEmail(params.get("username"));

        AccessToken accessToken = tokenService.generateAccessToken(clientDetails, person);
        RefreshToken refreshToken = tokenService.generateRefreshToken(clientDetails, accessToken);

        // Delete refresh token after one use (also the matching access token)
        if(rt != null) {
            this.tokenService.deleteAccessToken(rt.getAccessTokenId());
            this.tokenService.deleteRefreshToken(rt.getId());
        }

        return new ResponseEntity<>(new AccessTokenResponse(accessToken, refreshToken, clientDetails), HttpStatus.OK);
    }

    @RequestMapping(value = "/check_token", method = RequestMethod.GET)
    public ResponseEntity<CheckTokenResponse> checkToken(@RequestParam String token) {
        AccessToken accessToken = tokenService.tokenIsValid(token);
        ClientDetails clientDetails = clientDetailsService.get(accessToken.getClientId());

        Person person = null;
        if(accessToken.getUsername() != null) person = personService.getByEmail(accessToken.getUsername());

        return new ResponseEntity<>(new CheckTokenResponse(accessToken, clientDetails, person), HttpStatus.OK);
    }

}
