package nl.trifork.bank.authenticationms.DAO;

import nl.trifork.bank.authenticationms.model.oauth.ClientDetails;
import org.springframework.data.repository.CrudRepository;

public interface ClientDetailsDAO extends CrudRepository<ClientDetails, String> {

    ClientDetails findClientDetailsByClientId(String clientId);

}
