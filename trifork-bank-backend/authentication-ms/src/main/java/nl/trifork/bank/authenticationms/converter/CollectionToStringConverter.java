package nl.trifork.bank.authenticationms.converter;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Collection;

@Converter(autoApply = true)
public class CollectionToStringConverter implements AttributeConverter<Collection<String>, String> {
    @Override
    public String convertToDatabaseColumn(Collection<String> strings) {
        if(strings == null) return null;

        return StringUtils.collectionToCommaDelimitedString(strings);
    }

    @Override
    public Collection<String> convertToEntityAttribute(String s) {
        if(s == null) return null;

        return StringUtils.commaDelimitedListToSet(s);
    }
}
