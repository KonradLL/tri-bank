package nl.trifork.bank.authenticationms.DAO;

import nl.trifork.bank.authenticationms.model.oauth.AccessToken;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccessTokenDAO extends CrudRepository<AccessToken, Integer> {

    AccessToken findAccessTokenByClientIdAndUsername(String clientId, String username);

    List<AccessToken> findAccessTokensByClientIdAndUsername(String clientId, String username);

    AccessToken findAccessTokenByToken(String token);

}
