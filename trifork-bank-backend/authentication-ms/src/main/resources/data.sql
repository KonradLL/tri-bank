INSERT INTO oauth_client_details
(
  client_id,
  client_secret,
  scope,
  authorized_grant_types,
  authorities,
  access_token_validity,
  refresh_token_validity
)
VALUES
(
  1,
  'kRSOODqwtfH4gFbbDfkXUn3W7z3hhBeAmllj1bItz3RZ7gSM1KsQXzXf7PzoqMUk8EnN6bmwyLPL9TVWDy9SMKWFSQ3Az3dbrnMg',
  'user,admin',
  'password,refresh_token',
  'human',
  900,
  3600
)
ON CONFLICT DO NOTHING;

INSERT INTO oauth_client_details
(
  client_id,
  client_secret,
  scope,
  authorized_grant_types,
  authorities,
  access_token_validity,
  refresh_token_validity
)
VALUES
(
  2,
  'PQ6tY1StTfGrYPShsHNcSDqz4aSUPQsmNegZkmqfzbOa8Nt0VOnt4PNsabUqygWjHKOc1HF8CZ3gm9mFDWaXtxF5EjZs1s36YItr',
  'full',
  'client_credentials',
  'server',
  900,
  3600
)
ON CONFLICT DO NOTHING;
