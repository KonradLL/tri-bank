package nl.trifork.bank.accountms;

import nl.trifork.bank.accountms.DAO.AccountDAO;
import nl.trifork.bank.accountms.model.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AccountDAOTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountDAO accountDAO;

    private final String KEY_1 = "111111111";

    @Test
    public void testExample() throws Exception {
        Account account = new Account("Name", "Description");
        account.setKey(KEY_1);

        this.entityManager.persist(account);
        Account found = accountDAO.findAccountByKey(KEY_1);

        assertThat(found.getName()).isEqualTo(account.getName());
        assertThat(found.getDescription()).isEqualTo(account.getDescription());
    }

}
