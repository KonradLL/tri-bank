#!/bin/sh

docker tag account-ms konradll/account-ms:latest
docker push konradll/account-ms:latest

docker tag authentication-ms konradll/authentication-ms:latest
docker push konradll/authentication-ms:latest

docker tag config-ms konradll/config-ms:latest
docker push konradll/config-ms:latest

docker tag discovery-ms konradll/discovery-ms:latest
docker push konradll/discovery-ms:latest

docker tag person-ms konradll/person-ms:latest
docker push konradll/person-ms:latest

docker tag transfer-ms konradll/transfer-ms:latest
docker push konradll/transfer-ms:latest

docker tag zuul-ms konradll/zuul-ms:latest
docker push konradll/zuul-ms:latest