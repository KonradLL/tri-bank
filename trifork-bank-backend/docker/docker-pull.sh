#!/bin/sh
# $1 = $CI_COMMIT_REF_NAME, name of branch

docker pull konradll/account-ms:$1
docker tag konradll/account-ms:$1 account-ms

docker pull konradll/authentication-ms:$1
docker tag konradll/authentication-ms:$1 authentication-ms

docker pull konradll/config-ms:$1
docker tag konradll/config-ms:$1 config-ms

docker pull konradll/discovery-ms:$1
docker tag konradll/discovery-ms:$1 discovery-ms

docker pull konradll/person-ms:$1
docker tag konradll/person-ms:$1 person-ms

docker pull konradll/transfer-ms:$1
docker tag konradll/transfer-ms:$1 transfer-ms

docker pull konradll/zuul-ms:$1
docker tag konradll/zuul-ms:$1 zuul-ms