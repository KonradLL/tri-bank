#!/bin/sh
# $1 = $ACCESS_TOKEN

tee ./config-ms/gitclone.sh << EOF
cd /root
git clone -b root https://oauth2:$1@gitlab8.trifork.nl/students/configuration.git
EOF
