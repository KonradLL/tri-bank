#!/bin/bash


echo "spring.application.name=config-ms
server.port=8888
spring.cloud.config.server.git.uri=https://github.com/gijpie/config
spring.cloud.config.server.git.default-label=production
eureka.client.serviceUrl.defaultZone=http://discovery-ms:8761/eureka/" > \
config-ms/src/main/resources/bootstrap.properties

echo "
spring.cloud.config.uri=http://config-ms:8888" >> \
discovery-ms/src/main/resources/bootstrap.properties

echo "
eureka.client.serviceUrl.defaultZone=http://discovery-ms:8761/eureka/
spring.cloud.config.uri=http://config-ms:8888" >> \
account-ms/src/main/resources/bootstrap.properties

echo "
eureka.client.serviceUrl.defaultZone=http://discovery-ms:8761/eureka/
spring.cloud.config.uri=http://config-ms:8888" >> \
authentication-ms/src/main/resources/bootstrap.properties

echo "
eureka.client.serviceUrl.defaultZone=http://discovery-ms:8761/eureka/
spring.cloud.config.uri=http://config-ms:8888" >> \
person-ms/src/main/resources/bootstrap.properties

echo "
eureka.client.serviceUrl.defaultZone=http://discovery-ms:8761/eureka/
spring.cloud.config.uri=http://config-ms:8888" >> \
transfer-ms/src/main/resources/bootstrap.properties

echo "
eureka.client.serviceUrl.defaultZone=http://discovery-ms:8761/eureka/
spring.cloud.config.uri=http://config-ms:8888" >> \
zuul-ms/src/main/resources/bootstrap.properties
