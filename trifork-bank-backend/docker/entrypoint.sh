#!/bin/sh
while ! nc -z config-ms 8888 ; do
    echo "Waiting for the Config Server"
    sleep 3
done
while ! nc -z discovery-ms 8761 ; do
    echo "Waiting for the Eureka Server"
    sleep 3
done
