#!/bin/sh
# $1 = $CI_COMMIT_REF_NAME, name of branch

docker tag account-ms konradll/account-ms:$1
docker push konradll/account-ms:$1

docker tag authentication-ms konradll/authentication-ms:$1
docker push konradll/authentication-ms:$1

docker tag config-ms konradll/config-ms:$1
docker push konradll/config-ms:$1

docker tag discovery-ms konradll/discovery-ms:$1
docker push konradll/discovery-ms:$1

docker tag person-ms konradll/person-ms:$1
docker push konradll/person-ms:$1

docker tag transfer-ms konradll/transfer-ms:$1
docker push konradll/transfer-ms:$1

docker tag zuul-ms konradll/zuul-ms:$1
docker push konradll/zuul-ms:$1