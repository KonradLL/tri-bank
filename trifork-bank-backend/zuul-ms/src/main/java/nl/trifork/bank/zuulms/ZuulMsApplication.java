package nl.trifork.bank.zuulms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableEurekaClient
@SpringBootApplication
@EnableZuulProxy
public class ZuulMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulMsApplication.class, args);
	}

}
